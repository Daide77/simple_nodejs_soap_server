require('console-stamp')(console, 'yyyy-mm-dd  HH:MM:ss.l')           ;
const util  = require('util')                                         ;

/* edit me */
let port    = 8001                                                    ;
let path    = '/BookService'                                          ;
let wsdl    = 'SimpleWsdl.wsdl'


/* adjust with your soap methods */
var myService = {
  "BookService": {
    "BookServiceSOAP": {
      "GetBook": function(args, callback, headers, req) {
        console.log('DEBUG 1', headers);
        console.log('SOAP `reallyDeatailedFunction` request from ' + req.connection.remoteAddress), 
          callback({
            "tns:Book": {
              "ID": "1234",
              "Title": "Me pippo",
              "Author": "Mr pippo"
            }
        })
      },
      "AddBook": function(args, callback, headers, req) {
        console.log('DEBUG 1', headers);
        console.log('SOAP `reallyDeatailedFunction` request from ' + req.connection.remoteAddress),
          callback({
              "tns:Book": {
                "ID": "1234",
                "Title": "Me pippo",
                "Author": "Mr pippo"
              }
          })

      },
      "GetAllBooks": function(args, callback, headers, req) {
        console.log('DEBUG 1', headers);
        console.log('SOAP `reallyDeatailedFunction` request from ' + req.connection.remoteAddress),
          callback({
              "tns:Book": {
                "ID": "1234",
                "Title": "Me pippo",
                "Author": "Mr pippo"
              }
          })
      }
    }
  }
}

// https://www.npmjs.com/package/soap#soaplistenserver-path-services-wsdl-callback---create-a-new-soap-server-that-listens-on-path-and-provides-services 
var xml     = require('fs').readFileSync(wsdl, 'utf8')                ;
var assert  = require('assert')                                       ; 
var request = require('request')                                      ;
var soap    = require('soap')                                         ; 
var http    = require('http')                                         ;

var server  = http.createServer(function(request,response) {
    response.end('404: Not Found: ' + request.url)                    ;
});

server.listen( port , function () {
var soapL   = soap.listen( server, path, myService, xml, function()   {
  // console.log('Server started on ',port  );
  console.log( util.format('Server started on: %s path: %s'        , 
                            port                                   ,
                            path                                    ));

  soapL.on('request', function(request, methodName ){
    console.log(util.format(
                '#### SOAP request ##### method: %s'               ,
                methodName                                         ));
    console.log(JSON.stringify(request, null, 4));
  });

  soapL.on('response', function(response, methodName){
    console.log(' ## SOAP response ## ');
    // console.log(JSON.stringify(response, null, 4));
    // assert.equal( methodName, 'MyDarnMethod');
  });

 });

 soapL.addSoapHeader(function(methodName, args, req) {
  console.log('Adding response headers for method', methodName);
  /* you can add to response header too
  return {
    MyHeader1: "pippo",
    MyHeader2: "pluto"
  };
  */
 }); 

 soapL.authorizeConnection = function(req) {
   /* wanna check something from the request before even look at the request? */
   console.log('DEBUG SOAP auth ');
   console.log('IP      request from ' + req.connection.remoteAddress) ; 
   console.log('HEADERS request ') ; 
   console.dir(req.headers) ; 
   return true; // https://www.npmjs.com/package/basic-auth 
 }; 
 
 soapL.log = function(type, data) {
   /* log here what's is going on */
   console.log( "### DEBUG LOG ###" ) ;
   console.log( type,data ) ;
   console.log( "### FINE LOG ###" ) ;
 };

});

